#!/bin/bash

function show_help {
    echo "Script for running tests. Reuires root privileges"
    echo "rus.sh [-h] [-f] [-d DISPLAY]"
    echo
    echo "    -h         show this help message"
    echo "    -f         fast run: without installing dependencies"
    echo "    -d DISPLAY number of display"
    echo
}

FAST="0"
for arg in $*; do
    # try to find -h key in given parameters
    # can't grep /[-]{0,2}\bhelp\b|-h\b|\/[\?h]/ for some reason...
    if [ "$arg" = "-h" ]; then
        show_help
        exit
    elif [ "$arg" = "-f" ]; then
        FAST="1"
    fi
done

while [ "$1" != "-d" -a $# -ne 0 ]; do
    shift  # skip value
done
shift

calc() {
    awk "BEGIN { print "$*" }";
}

# setup variables
export ROOT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
export LOG_DIR="$ROOT_DIR/log"
export LOG_FILE_GLOB="$LOG_DIR/global.log"
export LOG_PATH_TEST="$LOG_DIR/tests.log"
export MAX_SLEEP_TIME=1
export AVRG_SLEEP_TIME="$(calc $MAX_SLEEP_TIME/2)"
export MIN_SLEEP_TIME="$(calc $MAX_SLEEP_TIME/10)"
LOG_FILE_SYS="$LOG_DIR/sys.log"
WORK_DIR="$ROOT_DIR/tests"
LOG_FILE_FULL="/dev/tty"
LOCAL_XAUTH_PATH="~/.Xauthority"
GLOGAL_XAUTH_PATH="/run/user/1000/gdm/Xauthority"
DEF_DISPLAY=":0"


echo "$(date) - Setting up..." >> $LOG_FILE_FULL

if [[ -e $LOG_DIR ]]; then
    echo "$(date) - Removing old logs..." >> $LOG_FILE_FULL
    rm -rf $LOG_DIR >> $LOG_FILE_FULL 2>> $LOG_FILE_FULL
fi
mkdir $LOG_DIR >> $LOG_FILE_FULL 2>> $LOG_FILE_FULL
echo "$(date) - Create log dir: '$LOG_DIR'" > $LOG_FILE_GLOB

if [[ $FAST -ne "0" ]]; then
    echo "$(date) - Skip dependencies installation."
else
    echo "$(date) - Installing dependencies..." >> $LOG_FILE_GLOB
    sudo apt-get -qy update >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
    sudo apt-get -qyf upgrade >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB

    sudo apt-get -qyf install xauth xserver-xorg scrot >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
    sudo apt-get -qyf install python3 python3-pip python3-venv >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
    sudo apt-get -qyf install xdotool >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB

    # There are troubles with installing python3-xlib <...> into virtual environment
    # python3 -m venv .venv >> $LOG_FILE_FULL
    # source .venv/bin/activate >> $LOG_FILE_FULL
    sudo apt-get -qyf install python3-xlib python3-tk python3-dev >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
    pip3 install -Ur requirements.txt >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB  # --no-cache

    sudo apt-get -qy autoremove >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
    sudo apt-get -qy autoclean >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
fi


echo "$(date) - Setup X11 variables..." >> $LOG_FILE_GLOB
if [[ -n $1 ]]; then
    export DISPLAY=$1
elif [[ ! -n $DISPLAY ]]; then
    echo "$(date) - Use default value for DISPLAY: '$DEF_DISPLAY'" >> $LOG_FILE_GLOB
    export DISPLAY=$DEF_DISPLAY
fi
echo "$(date) - Setup DISPLAY name: '$DISPLAY'" >> $LOG_FILE_GLOB

if [[ -f $LOCAL_XAUTH_PATH ]]; then
    export XAUTHORITY=$LOCAL_XAUTH_PATH
elif [[ -f $GLOGAL_XAUTH_PATH ]]; then
    export XAUTHORITY=$GLOGAL_XAUTH_PATH
else
    # assume this command exports XAUTHORITY
    sudo xauth generate $DISPLAY . >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB  # trusted
fi
echo "$(date) - Use '$XAUTHORITY' for xauth configuration" >> $LOG_FILE_GLOB
sudo chmod a+rwx $XAUTHORITY >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB


echo "$(date) - Collecting system info..." >> $LOG_FILE_GLOB
echo "===== System info =====" > $LOG_FILE_SYS
uname -a >> $LOG_FILE_SYS 2>> $LOG_FILE_GLOB
lsb_release -a >> $LOG_FILE_SYS 2>> $LOG_FILE_GLOB
echo "===== X11 auth info =====" >> $LOG_FILE_SYS
xauth info >> $LOG_FILE_SYS
echo "===== Toolchain info =====" >> $LOG_FILE_SYS
xdotool version >> $LOG_FILE_SYS
scrot -v >> $LOG_FILE_SYS
python3 -V >> $LOG_FILE_SYS


echo "$(date) - Start testing..." >> $LOG_FILE_GLOB
python3 log_system_info.py >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
pytest -v $WORK_DIR >> $LOG_FILE_FULL 2>> $LOG_FILE_GLOB
echo "$(date) - End testing." >> $LOG_FILE_GLOB

echo "$(date) - Finish." >> $LOG_FILE_FULL
# summary is provided by pytest
