import subprocess

from config import XDOTOOL_PATH
from utils.log import log


class Xdotool:
    @staticmethod
    @log()
    def search(pid: str=None,
               classname: str=None,
               name: str=None,
               onlyvisible: bool=True,
               check_all: bool=True,
               sync: bool=False,
               *args,
               **kwargs) -> subprocess.CompletedProcess:
        command = [XDOTOOL_PATH, "search"]
        command.extend(args)

        if pid is not None:
            if not isinstance(pid, str):
                pid = str(pid)
            command.append("--pid")
            command.append(pid)
        elif classname:
            command.append("--classname")
            command.append(classname)
        elif name:
            command.append("--name")
            command.append(name)

        if sync:
            command.append("--sync")
        if check_all and "--any" not in args:
            command.append("--all")
        if onlyvisible:
            command.append("--onlyvisible")

        return subprocess.run(
            command,
            stderr=subprocess.PIPE,
            stdout=subprocess.PIPE,
            **kwargs
        )

    @staticmethod
    @log()
    def get_decoded_output_as_list(output_stream: object,
                                   encoding: str="ascii",
                                   sep: str='\n') -> list:
        if isinstance(output_stream, bytes):
            return list(filter(
                lambda s: s,
                output_stream.decode(encoding).split(sep)
            ))
        else:
            return [line.decode(encoding).strip(sep)
                    for line in output_stream.readlines() if line]
