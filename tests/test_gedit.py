import logging
import os
import subprocess
from time import sleep

import pyautogui as aut
import pytest

from config import GEDIT_PATH, SLEEP_TIME, TEST_FILE_NAME, XDOTOOL_PATH
from tests.xdotool import Xdotool
from utils.log import Log, Logger, log, timer


SLEEP_TIME_MIN = SLEEP_TIME / 10
SLEEP_TIME_AVRG = SLEEP_TIME / 2
SLEEP_TIME_MAX = SLEEP_TIME * 2

aut.PAUSE = SLEEP_TIME_MIN


class Gedit:
    def __init__(self, path: str) -> None:
        self._path = path
        self._proc = None
        self._xid = None
        self._exit_code = None

    @property
    def xid(self) -> str:
        return self._xid

    @timer()
    def popen(self) -> subprocess.Popen or None:
        if self._proc is None:
            self._proc = subprocess.Popen(
                [self._path],
                stderr=subprocess.PIPE,
                stdout=subprocess.PIPE
            )
            sleep(SLEEP_TIME)
            self._proc.poll()
            return self._proc

    @timer()
    def get_xid(self) -> subprocess.CompletedProcess or None:
        if self._xid is None:
            search_proc = Xdotool.search(pid=self._proc.pid)
            sleep(SLEEP_TIME_AVRG)

            search_res = Xdotool.get_decoded_output_as_list(search_proc.stdout)
            assert len(search_res) == 1
            self._xid = search_res[0]

            return search_proc

    @timer()
    def activate(self) -> subprocess.CompletedProcess or None:
        if self._proc is not None:
            activate_proc = subprocess.run([
                XDOTOOL_PATH, "windowactivate", "--sync", self._xid
            ], stderr=subprocess.PIPE, stdout=subprocess.PIPE)
            sleep(SLEEP_TIME_MIN)
            return activate_proc

    @timer()
    def pclose(self) -> int:
        if self._proc is not None:
            self._proc.terminate()
            self._exit_code = self._proc.wait()
            self._proc = None
            self._xid = None
        return self._exit_code


@pytest.fixture(scope="module")
def gedit() -> Gedit:
    return Gedit(path=GEDIT_PATH)


class TestGedit:
    @pytest.fixture(autouse=True)
    def open_gedit(self, request: object, gedit: Gedit) -> None:
        with Log(request.function.__name__, self._logger, level=logging.INFO):
            gedit_proc = gedit.popen()
            if gedit_proc is not None:
                self._logger.debug("gedit pid: '%s'", gedit_proc.pid)
                assert gedit_proc.pid
                assert gedit_proc.returncode is None

            self._logger.debug("searching for gedit xid (window id)...")
            get_xid_proc = gedit.get_xid()
            if get_xid_proc is not None:
                err_msg = Xdotool.get_decoded_output_as_list(
                    get_xid_proc.stderr
                )
                if err_msg:
                    self._logger.warning("\n%s", "\n".join(err_msg))
                assert not get_xid_proc.returncode
                assert not err_msg
                self._logger.debug("gedit xid: %d", gedit.xid)

            activate_proc = gedit.activate()
            if activate_proc is not None:
                err_msg = Xdotool.get_decoded_output_as_list(
                    activate_proc.stderr
                )
                if err_msg:
                    self._logger.warning("\n%s", "\n".join(err_msg))
                self._logger.debug("windowactivate returncode: %d",
                                activate_proc.returncode)
                assert not activate_proc.returncode

            yield

            self._logger.debug("Terminate gedit")
            exit_code = gedit.pclose()
            self._logger.debug("Gedit exit code: %d", exit_code)

    def setup_class(self):
        self._logger = Logger(self.__class__.__name__)
        self.file_content = "Hello, world!"
        self._path = TEST_FILE_NAME
        self._logger.debug("Start testing...")

    def teardown_class(self):
        self._logger.debug("End testing.")

    def cwd_is_empty(self) -> bool:
        if os.path.exists(self._path):
            self._logger.debug("Remove old test file.")
            os.remove(self._path)
            sleep(SLEEP_TIME_MIN)
        return not os.path.exists(self._path)

    @log()
    def check_file_content(self,
                           content: str,
                           path: str=None) -> bool:
        if not path:
            path = self._path
        if not os.path.exists(path):
            self._logger.error("File '%s' isn't exists", path)
            return False
        with open(path, 'r') as fin:
            line = fin.readline().strip()
            if line != content:
                self._logger.error(
                    "Content missmatch: '%s' vs '%s'", content, line
                )
                return False
        return True

    @timer()
    def test_save_text_to_file(self):
        assert self.cwd_is_empty()

        aut.typewrite(self.file_content)
        aut.press("return")

        aut.hotkey("ctrl", "s")
        sleep(SLEEP_TIME_AVRG)

        aut.typewrite(self._path)

        aut.press("return")
        sleep(SLEEP_TIME_AVRG)

        assert self.check_file_content(self.file_content)

    @timer()
    def test_save_text_to_file_on_exit(self):
        assert self.cwd_is_empty()

        aut.typewrite(self.file_content)
        aut.press("return")

        aut.hotkey("alt", "f4")
        sleep(SLEEP_TIME_AVRG)

        aut.press("return")
        sleep(SLEEP_TIME_AVRG)

        aut.typewrite(self._path)

        aut.press("return")
        sleep(SLEEP_TIME_AVRG)

        assert self.check_file_content(self.file_content)

    @timer()
    def test_rewrite_text_to_file(self):
        updated_content = "Hello again!"
        assert self.cwd_is_empty()

        aut.typewrite(self.file_content)
        aut.press("return")

        aut.hotkey("ctrl", "s")
        sleep(SLEEP_TIME_AVRG)

        aut.typewrite(self._path)

        aut.press("return")
        sleep(SLEEP_TIME_AVRG)

        assert self.check_file_content(self.file_content)

        aut.hotkey("ctrl", "a")
        aut.press("del")

        aut.typewrite(updated_content)
        aut.press("return")

        aut.hotkey("ctrl", "shift", "s")
        sleep(SLEEP_TIME_AVRG)

        aut.press("return")
        sleep(SLEEP_TIME_AVRG)

        aut.press("return")
        sleep(SLEEP_TIME_AVRG)

        assert self.check_file_content(updated_content)
