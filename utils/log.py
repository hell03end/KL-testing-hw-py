import logging
import traceback
from functools import wraps
from time import time
from typing import Callable

try:
    from config import LOG_FORMAT, LOG_LEVEL, LOG_MODE, LOG_PATH
except ImportError:
    LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    LOG_LEVEL = logging.INFO
    LOG_MODE = "w"
    LOG_PATH = os.path.join(".", "log")


logging.basicConfig(
    format=LOG_FORMAT,
    level=LOG_LEVEL,
    filemode=LOG_MODE,
    filename=LOG_PATH
)
LOGGER = logging.getLogger("$global")


def Logger(name: str) -> logging.RootLogger:
    """ Creates named logger instance """
    return logging.getLogger(name)


class Log:
    """
        Context manager for logging some event happening

        Handling exceptions (log them).
    """

    def __init__(self,
                 case_name: str,
                 logger: logging.RootLogger=LOGGER,
                 level: int=logging.DEBUG,
                 **kwargs):
        self.log = logger
        self._name = case_name
        self._level = level
        self._enter_msg = kwargs.pop("enter_msg", "ENTERING::")
        self._exit_msg = kwargs.pop("exit_msg", "EXITING::")
        self._exc_msg = kwargs.pop("exc_msg", "")
        self._silent = kwargs.pop("silent", False)

    def __enter__(self) -> logging.RootLogger or object:
        """ Returns it's logger instance of self if silent """
        if self._silent:
            return self

        message = ("%s%s", self._enter_msg, self._name)
        if self._level > logging.DEBUG:
            self.log.info(*message)
        else:
            self.log.debug(*message)

        return self.log

    def __exit__(self,
                 exc_type: object=None,
                 exc_val: object=None,
                 tb: object=None) -> None:
        """ Handling exceptions (log them) """
        if exc_type is not None:
            self.log.error(
                "%s\n%s%s: %s\n",
                self._exc_msg,
                "\n".join([s.strip(r"\n") for s in traceback.format_tb(tb)]),
                exc_type.__name__,
                exc_val
            )

        if self._silent:
            return

        message = ("%s%s", self._exit_msg, self._name)
        if self._level > logging.DEBUG:
            self.log.info(*message)
        else:
            self.log.debug(*message)


class _FuncLog(Log):
    """ Context manager for logging function call """
    def __init__(self,
                 case_name: str,
                 logger: logging.RootLogger,
                 level: int=logging.DEBUG):
        super(_FuncLog, self).__init__(
            case_name=case_name,
            logger=logger,
            level=level,
            enter_msg="===> ",
            exit_msg="<--- "
        )


def log(logger: logging.RootLogger=LOGGER,
        log_result: bool=True,
        log_args: bool=True,
        log_kwargs: bool=True,
        name: str=None,
        level: int=logging.DEBUG) -> Callable:
    """ Returns decorator for logging function/method behavior """
    def decor(func: Callable) -> Callable:
        func_name = name
        if not func_name:
            func_name = func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs) -> object:
            with _FuncLog(func_name, logger, level) as func_log:
                if log_args:
                    for arg in args:
                        func_log.debug("()::%s", arg)
                if log_kwargs:
                    for key, value in kwargs.items():
                        func_log.debug(r"{}::%s=%s", key, value)

                result = func(*args, **kwargs)
                if log_result:
                    func_log.debug("RETURN(%s)::%s", func_name, result)
            return result
        return wrapper
    return decor


class Timer(Log):
    """ Context manager for events duration """
    def __init__(self,
                 logger: logging.RootLogger=LOGGER,
                 level: int=logging.DEBUG,
                 case_name: str=None):
        self._start_time = None
        super(Timer, self).__init__(
            case_name=case_name,
            logger=logger,
            level=level,
            enter_msg=None,
            exit_msg=None,
            silent=True
        )

    def __enter__(self) -> object:
        self._start_time = time()
        return super(Timer, self).__enter__()

    def __exit__(self,
                 exc_type: object=None,
                 exc_val: object=None,
                 tb: object=None) -> None:
        message = (
            "Elapsed time[%s]: %s",
            self._name,
            time() - self._start_time
        )
        if self._level > logging.DEBUG:
            self.log.info(*message)
        else:
            self.log.debug(*message)

        return super(Timer, self).__exit__(exc_type, exc_val, tb)


def timer(logger: logging.RootLogger=LOGGER,
          level: int=logging.DEBUG,
          name: str=None) -> Callable:
    """ Returns decorator for logging function/method execution time """
    def decor(func: Callable) -> Callable:
        func_name = name
        if not func_name:
            func_name = func.__name__

        @wraps(func)
        def wrapper(*args, **kwargs) -> object:
            with Timer(logger, level, func_name) as timer:
                return func(*args, **kwargs)
        return wrapper
    return decor
