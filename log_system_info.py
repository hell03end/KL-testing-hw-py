import os
import platform
import sys


def get_system_info_str() -> str:
    """ Returns string, contains main information about system """
    return ", ".join((platform.platform(), sys.getfilesystemencoding()))


def get_python_info_str() -> str:
    """ Returns string with info about current python interpreter """
    return "{impl} {v} {release} ({build}) [{compiler}] ({enc})::{path}".format(
        impl=platform.python_implementation(),
        v=platform.python_version(),
        release=sys.implementation.version.releaselevel,
        build=", ".join(platform.python_build()),
        compiler=platform.python_compiler(),
        enc=sys.getdefaultencoding(),
        path=sys.executable
    )


def get_current_user_str() -> str:
    """ Returns current user name """
    return platform.node()


if __name__ == "__main__":
    from utils.log import LOGGER as logger
    logger.info("System info:\n%s", get_system_info_str())
    logger.info("Python info:\n%s", get_python_info_str())
    logger.info("Current user: %s", get_current_user_str())
