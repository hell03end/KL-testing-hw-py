import logging
import os

LOG_LEVEL = logging.INFO
LOG_DIR = os.environ.get("LOG_DIR", os.path.join(".", "log"))
LOG_FILE = "tests.log"
LOG_PATH = os.path.join(LOG_DIR, LOG_FILE)
LOG_MODE = "a"
LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"

SLEEP_TIME = 1

XDOTOOL_PATH = os.path.join("{}usr".format(os.path.sep), "bin", "xdotool")
GEDIT_PATH = os.path.join("{}usr".format(os.path.sep), "bin", "gedit")
TEST_FILE_NAME = "results.tmp"
